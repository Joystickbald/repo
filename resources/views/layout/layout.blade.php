<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{config('app.name','kuba.dev')}}</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
   @yield('content')
</body>
</html>