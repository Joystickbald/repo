<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	
	return 'Hello World';
});

Route::get('/controller', 'PagesController@index');
Route::get('/google', 'PagesController@google');
Route::get('/strona', 'PagesController@strona');

/*
Route::get('/about', function () {
	
    return view('about', ["name2" => "cześć"])->with("name", "test");
});

Route::get('/strona', function () {
	
	return view('pages.strona');
});

Route::get('/google', function () {
	
	return view('welcome');
});

Route::get('/users/{id}/{name}', function ($id, $name) {
	
    //return 'złapane id to = '.$id;
    return 'to jest user ' . $name . ' który ma id = ' . $id;
});
*/