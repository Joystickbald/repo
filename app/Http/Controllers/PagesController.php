<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        //$title = 'Witaj w Laravel!!!';
        $data = array
        (
            'title' => 'Witaj w Laravel!!!',
            'services' => ['Web Design', 'Programing', 'SEO']
        );
        //return view('pages.index', compact('title'));
        return view('pages.index') -> with($data);
    }

    public function strona()
    {
        $title = 'Witaj na Stronie';
        return view('pages.strona', compact('title'));
    }

    public function google()
    {
        return view('pages.welcome');
    }
}
